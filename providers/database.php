<?php
//$app['db.options'] = array(
//    'driver'   => 'pdo_mysql',
//    'host' => 'localhost',
//    'dbname' => 'mydbname',
//    'user' => 'mydbuser',
//    'password' => 'mydbpassword',
//);
$app['db.options'] = array(
    'driver'   => 'pdo_sqlite',
    'path'     => __DIR__.'/../db/sandbox.db',
    //'url' => 'sqlite:///db/sandbox.db',
);
