<?php

namespace Lorga\Lor\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embedded;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Entity;

/** @Entity */
class MapField
{

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @Embedded(class="MapFieldPosition", columnPrefix="pos_")
     * @var MapFieldPosition
     */
    private $coordinates;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $type;
    
    /**
     * @Column(type="boolean")
     * @var bool
     */
    private $habitable;
}
