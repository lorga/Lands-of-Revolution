<?php
use Silex\Provider;

require_once __DIR__ . '/vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;


$app->register(new Provider\DoctrineServiceProvider());
$app->register(new \Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider(), [
    'orm.em.options' => [
        'mappings' => [
            [
                'type' => 'annotation',
                'namespace' => 'Lorga\Lor\Entity',
                'path' => __DIR__ . '/src/Entity',
            ],
        ],
    ],
]);
//$app['security.firewalls'] = [];
$app->register(new Provider\SecurityServiceProvider());
$app->register(new Provider\RememberMeServiceProvider());
$app->register(new Provider\SessionServiceProvider());
$app->register(new Provider\ServiceControllerServiceProvider());
// not there anymore $app->register(new Provider\UrlGeneratorServiceProvider());
// use RoutingServiceProvider https://github.com/silexphp/Silex-Skeleton/issues/39
$app->register(new Provider\TwigServiceProvider());
$app->register(new Provider\SwiftmailerServiceProvider());
// Mailer config. See http://silex.sensiolabs.org/doc/providers/swiftmailer.html
$app['swiftmailer.options'] = [];

$app->register(new Mustache\Silex\Provider\MustacheServiceProvider, array(
    'mustache.path' => __DIR__.'/res/views',
    'mustache.options' => array(
        'cache' => __DIR__.'/tmp/cache/mustache',
    ),
));

$app['render_service'] = function() use ($app) {
    return function ($view, $params = [], $layout = null) use ($app) {
        $params['user'] = [
            'isLoggedIn' => $app['user.manager']->isLoggedIn(),
            'name' => $app['user.manager']->getCurrentUser()->getUsername(),
            //'name' => $app['user.manager']->getCurrentUser()->getRealUsername(),
        ];
        $params['body'] = $app['mustache']->render($view, $params);
        return $app['mustache']->render('layout', $params);
    };
};

$simpleUserProvider = new SimpleUser\UserServiceProvider();
$app->register($simpleUserProvider);



// Mount the user controller routes:
$app->mount('/user', $simpleUserProvider);


require_once __DIR__ . '/providers/security.php';
require_once __DIR__ . '/providers/database.php';
require_once __DIR__ . '/providers/actions.php';
