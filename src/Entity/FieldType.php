<?php


namespace Lorga\Lor\Entity;

class FieldType
{
    const TYPE_WATER = 0;
    const TYPE_PLAIN = 1;
    const TYPE_FOREST = 2;
    const TYPE_MOUNTAIN_STONE = 3;
    const TYPE_MOUNTAIN_IRON = 4;

    const NAMES = [
        self::TYPE_WATER => 'water',
        self::TYPE_PLAIN => 'plain',
        self::TYPE_FOREST => 'forest',
        self::TYPE_MOUNTAIN_STONE => 'stone',
        self::TYPE_MOUNTAIN_IRON => 'iron',
    ];
    
    public static function getName($type)
    {
        $names = self::NAMES;
        return $names[$type];
    }
}
