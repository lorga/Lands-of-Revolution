<?php

namespace Lorga\Lor\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embeddable;

/**
 * @Embeddable
 */
class MapFieldPosition
{

    /**
     * @Column(type="integer")
     * @var int
     */
    private $x;
    /**
     * @Column(type="integer")
     * @var int
     */
    private $y;

    /**
     * MapFieldPosition constructor.
     *
     * @param int $x
     * @param int $y
     */
    public function __construct( $x = null, $y = null)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }
}
