<?php

/** @var Silex\Application $app */

$app->get('/', function () use ($app) {
    $renderService = $app['render_service'];
    return $renderService('home');
});

$app->get('/test', function () use ($app) {
    $result = $app['db']->fetchAll('SELECT * FROM users');
    var_dump($result);
    return "Hello World";
});
